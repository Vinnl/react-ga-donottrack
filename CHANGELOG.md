# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1] - 2020-04-26

### Bugs fixed

- React no longer complains about the `trackerNames` property that was added to <OutboundLink>s in
  react-ga v2.6.0. This property would be passed on the to the <a> element that would be rendered
  instead when the user has DoNotTrack enabled, but that element does not support such a property.

## [1.0.0] - 2018-12-10

### New features

- Initial public release as separate package
