react-ga-donottrack
======
A wrapper of [react-ga](https://www.npmjs.com/package/react-ga) that only adds Google Analytics if the user has not requested tracking to be disabled through [`DoNotTrack`](https://developer.mozilla.org/en-US/docs/Web/API/Navigator/doNotTrack). This package was mainly created to be able to avoid requests to Google's servers if DoNotTrack is enabled.

Read the documentation of react-ga to learn how to use this module. The functions included in react-ga are only executed if `navigator.DoNotTrack` is not set, or is set to something other than `1`. The `<OutboundLink>` component outputs a regular `<a>` if `navigator.DoNotTrack` is set to `1`.

Unfortunately, [plugins](https://www.npmjs.com/package/react-ga#reactgapluginrequirename-options) are not supported yet, although contributions to add that are welcome.

This project also includes TypeScript type definitions.

## Installation

```bash
npm install react-ga-donottrack --save
```

## Usage

```javascript
import * as ReactGA from 'react-ga-donottrack';

ReactGA.initialize('UA-000000-01');
ReactGA.pageview(window.location.pathname + window.location.search);
```

## API

See https://www.npmjs.com/package/react-ga#api

Note that plugins are not currently supported.

## License

MIT © [Vincent Tunru](https://vincenttunru.com)
